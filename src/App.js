import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Layout from "./containers/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import AddNewPost from "./containers/AddNewPost/AddNewPost";
import FullPost from "./containers/FullPost/FullPost";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Posts} />
          <Route path="/post/:id" exact component={FullPost} />
          <Route path="/posts/add-new-post" exact component={AddNewPost} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route render={() => <h1 style={{textAlign: 'center'}}>Page not found</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
