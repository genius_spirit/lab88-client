import {
  CREATE_POST_FAILURE, GET_FULL_POST_FAILURE,
  GET_FULL_POST_SUCCESS,
  GET_POSTS_FAILURE,
  GET_POSTS_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  posts: [],
  error: null,
  fullPost: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_POST_FAILURE:
      return {...state, error: action.error};
    case GET_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case GET_POSTS_FAILURE:
      return {...state, error: action.error};
    case GET_FULL_POST_SUCCESS:
      return {...state, fullPost: action.post};
    case GET_FULL_POST_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;