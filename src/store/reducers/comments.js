import {
  ADD_NEW_COMMENT_FAILURE,
  ADD_NEW_COMMENT_SUCCESS,
  GET_COMMENTS_FAILURE,
  GET_COMMENTS_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  comments: [],
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_COMMENT_SUCCESS:
      return {...state, error: null};
    case ADD_NEW_COMMENT_FAILURE:
      return {...state, error: action.error};
    case GET_COMMENTS_SUCCESS:
      return {...state, comments: action.data};
    case GET_COMMENTS_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;