import axios from "../../axios-api";
import { push } from "react-router-redux";
import {
  CREATE_POST_FAILURE,
  CREATE_POST_SUCCESS, GET_FULL_POST_FAILURE,
  GET_FULL_POST_SUCCESS,
  GET_POSTS_FAILURE,
  GET_POSTS_SUCCESS
} from "./actionTypes";

const createPostSuccess = () => {
  return {type: CREATE_POST_SUCCESS};
};

const createPostFailure = error => {
  return {type: CREATE_POST_FAILURE, error};
};

export const fetchPost = postData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    return axios.post('/posts', postData, {headers}).then(
      response => {
        dispatch(createPostSuccess());
        dispatch(push('/'))
      },
      error => {
        dispatch(createPostFailure(error));
      }
    )
  }
};

const getPostsSuccess = (posts) => {
  return {type: GET_POSTS_SUCCESS, posts};
};

const getPostsFailure = error => {
  return {type: GET_POSTS_FAILURE, error};
};

export const getPosts = () => {
  return dispatch => {
    return axios.get('/posts').then(
      response => {
        dispatch(getPostsSuccess(response.data));
      },
      error => {
        dispatch(getPostsFailure(error));
      }
    );
  };
};

const getFullPostSuccess = post => {
  return {type: GET_FULL_POST_SUCCESS, post};
};

const getFullPostFailure = error => {
  return {type: GET_FULL_POST_FAILURE, error};
};

export const getFullPost = id => {
  return dispatch => {
    return axios.get(`/posts/${id}`).then(
      response => {
        dispatch(getFullPostSuccess(response.data));
      },
      error => {
        dispatch(getFullPostFailure(error));
      }
    );
  };
};