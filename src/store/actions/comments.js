import axios from '../../axios-api';
import {
  ADD_NEW_COMMENT_FAILURE,
  ADD_NEW_COMMENT_SUCCESS,
  GET_COMMENTS_FAILURE,
  GET_COMMENTS_SUCCESS
} from "./actionTypes";

const addNewCommentSuccess = (data) => {
  return {type: ADD_NEW_COMMENT_SUCCESS, data};
};

const addNewCommentFailure = error => {
  return {type: ADD_NEW_COMMENT_FAILURE, error};
};

export const addNewComment = (data, id) => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    return axios.post('/comments', data, {headers}).then(
      response => {
        dispatch(addNewCommentSuccess(response.data));
        dispatch(getComments(id))
      },
      error => {
        dispatch(addNewCommentFailure(error.response.data));
      }
    );
  };
};

const getCommentsSuccess = data => {
  return {type: GET_COMMENTS_SUCCESS, data};
};

const getCommentsFailure = error => {
  return {type: GET_COMMENTS_FAILURE, error};
};

export const getComments = id => {
  return dispatch => {
    return axios.get(`/comments/${id}`).then(
      response => {
        dispatch(getCommentsSuccess(response.data));
      }, error => {
        dispatch(getCommentsFailure(error));
      }
    );
  };
};