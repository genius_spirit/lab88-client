import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Media } from "react-bootstrap";

import config from "../../config";
import notFound from "../../assets/images/not_found.png";

const PostItem = props => {
  return (
    <Media style={{ padding: "10px" }}>
      <Media.Left>
        <img
          alt={props.image}
          width={100}
          height={100}
          src={
            props.image ? config.apiUrl + "/uploads/" + props.image : notFound
          }
        />
      </Media.Left>
      <Media.Body>
        <Media.Heading>
          <p>{props.datetime + " by " + props.user.username}</p>
        </Media.Heading>
        <Link to={"/post/" + props.id}>
          <strong>{props.title}</strong>
        </Link>
      </Media.Body>
    </Media>
  );
};

PostItem.proptypes = {
  id: PropTypes.string.isRequired,
  image: PropTypes.string,
  datetime: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default PostItem;
