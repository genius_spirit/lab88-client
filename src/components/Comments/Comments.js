import React from "react";
import { ListGroupItem, Panel } from "react-bootstrap";
import { Header } from "semantic-ui-react";

const Comments = ({comments}) => {
  return (
    <ListGroupItem>
      <Panel.Body>
        <Header as='h3' dividing>Comments</Header>
        {comments ?
          comments.map(item => (
            <Panel key={item._id}>
              <Panel.Body>
                <strong>{item.user.username + ' - '}</strong>
                {item.title}
              </Panel.Body>
            </Panel>
          )) : null}
      </Panel.Body>
    </ListGroupItem>
  );
};

export default Comments;
