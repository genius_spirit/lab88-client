import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  Alert,
  Button,
  Col,
  ControlLabel,
  Form,
  FormControl,
  FormGroup,
  ListGroupItem,
  Panel
} from "react-bootstrap";
import { Header } from "semantic-ui-react";
import { addNewComment } from "../../store/actions/comments";

class AddCommentForm extends Component {

  state = {
    comment: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = {post: this.props.id, title: this.state.comment};

    this.props.addNewComment(formData, this.props.id);
  };

  render() {
    return(
       <Fragment>
         <ListGroupItem>
           <Panel.Body>
             <Header as='h4' dividing>Add new Comment</Header>
             <Form horizontal onSubmit={this.submitFormHandler}>
               {this.props.error && (
                 <Alert bsStyle="danger">{this.props.error}</Alert>
               )}
               <FormGroup controlId="commentTitle">
                 <Col componentClass={ControlLabel} sm={2}>Comment</Col>
                 <Col sm={10}>
                   <FormControl
                     componentClass="textarea"
                     placeholder="Enter comment text"
                     name="comment"
                     value={this.state.comment}
                     onChange={this.inputChangeHandler}
                   />
                 </Col>
               </FormGroup>
               <FormGroup>
                 <Col smOffset={2} sm={10}>
                   <Button bsStyle="primary" type="submit">Save</Button>
                 </Col>
               </FormGroup>
             </Form>
           </Panel.Body>
         </ListGroupItem>
       </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addNewComment: (data, id) => dispatch(addNewComment(data, id)),
  }
};

export default connect(null, mapDispatchToProps)(AddCommentForm);