import React, { Fragment } from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

const UserMenu = ({ user, logout }) => {
  return (
    <Fragment>
      <Menu.Item>
        <Link to="/posts/add-new-post">Add new post</Link>
      </Menu.Item>
      <Menu.Menu position="right">
        <Menu.Item>Hello, {user.username}</Menu.Item>
        <Menu.Item onClick={logout}>Logout</Menu.Item>
      </Menu.Menu>
    </Fragment>
  );
};

export default UserMenu;
