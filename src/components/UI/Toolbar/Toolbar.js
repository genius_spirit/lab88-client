import React from "react";
import { Menu } from "semantic-ui-react";

import UserMenu from "./Menus/UserMenu";
import GuestMenu from "./Menus/GuestMenu";
import { Link } from "react-router-dom";

const Toolbar = ({user, logout}) => {
  return (
    <Menu
      style={{
        marginBottom: "30px",
        borderTopLeftRadius: "0",
        borderTopRightRadius: "0"
      }}
    >
      <Menu.Item style={{ color: "#333", fontSize: "16px"}}>
        <Link to="/">
          Forum
        </Link>
      </Menu.Item>
      {user ? <UserMenu user={user} logout={logout}/> : <GuestMenu/>}
    </Menu>
  );
};

export default Toolbar;
