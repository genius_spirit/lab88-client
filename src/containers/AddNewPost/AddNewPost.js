import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { PageHeader } from "react-bootstrap";
import PostForm from "../../components/PostForm/PostForm";
import { fetchPost } from "../../store/actions/posts";

class AddNewPost extends Component {

  render() {
    return (
      <Fragment>
        <PageHeader>Add new post</PageHeader>
        <PostForm onSubmit={this.props.fetchPost}/>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchPost: postData => dispatch(fetchPost(postData))
  };
};


export default connect(null, mapDispatchToProps)(AddNewPost);
