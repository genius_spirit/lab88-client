import React from "react";
import { connect } from "react-redux";
import { NotificationContainer } from "react-notifications";
import { Container } from "semantic-ui-react";

import Toolbar from "../../components/UI/Toolbar/Toolbar";
import { logoutUser } from "../../store/actions/users";

import 'react-notifications/lib/notifications.css';

const Layout = props => {
  return (
    <Container>
      <NotificationContainer/>
      <header>
        <Toolbar user={props.user} logout={props.logoutUser}/>
      </header>
      <main>
        {props.children}
      </main>
    </Container>
  );
};

const mapStateToProps = state => {
  return {
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});


export default connect(mapStateToProps, mapDispatchToProps)(Layout);

