import React, { Component } from "react";
import { connect } from "react-redux";
import { getPosts } from "../../store/actions/posts";
import PostItem from "../../components/PostItem/PostItem";
import { Panel } from "react-bootstrap";

class Posts extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    return (
      <React.Fragment>
        {this.props.posts.map(item => (
          <Panel key={item._id}>
            <PostItem
              id={item._id}
              image={item.image}
              datetime={item.datetime}
              user={item.user}
              title={item.title}
            />
          </Panel>
        ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts.posts,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPosts: () => dispatch(getPosts())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
