import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { ListGroup, ListGroupItem, Panel } from "react-bootstrap";
import { getFullPost } from "../../store/actions/posts";

import AddCommentForm from "../../components/AddCommentForm/AddCommentForm";
import { getComments } from "../../store/actions/comments";
import Comments from "../../components/Comments/Comments";

class FullPost extends Component {
  componentDidMount() {
    this.props.getFullPost(this.props.match.params.id);
    this.props.getComments(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        {this.props.fullPost ? (
          <Panel>
            <Panel.Heading>
              {this.props.fullPost.title + ", " + this.props.fullPost.datetime}
            </Panel.Heading>
            <ListGroup>
              <ListGroupItem>
                <Panel.Body> {this.props.fullPost.description}</Panel.Body>
              </ListGroupItem>
              <Comments comments={this.props.comments} />
              {this.props.user && (
                <AddCommentForm
                  error={this.props.error}
                  id={this.props.match.params.id}
                />
              )}
            </ListGroup>
          </Panel>
        ) : null}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    fullPost: state.posts.fullPost,
    user: state.users.user,
    error: state.comments.error,
    comments: state.comments.comments
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getFullPost: id => dispatch(getFullPost(id)),
    getComments: id => dispatch(getComments(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FullPost);
